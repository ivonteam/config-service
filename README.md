# README #

This README would normally document whatever steps are necessary to get your application up and running.

#### Pushing content to an existing repo - instead of using 'git clone to pull' ###

* git init
* git add .
* git commit -m "initial commit"
* git remote add origin git@bitbucket.org:ivonteam/config-service.git
* git branch --set-upstream-to=origin/master master
* git pull --allow-unrelated-histories
* git push

OR

* git init
* git add .
* git commit -m "initial commit"
* git remote add origin git@bitbucket.org:ivonteam/config-service.git
* git push -u origin master

### Dockerisation ###

docker build -t spring-cloud/config-service .
docker run -p 8888:8888 -t spring-cloud/config-service

### Docker Commands ####
* docker images
* docker build -t spring-cloud/config-service .
* docker rmi spring-cloud/config-service
* docker ps
* docker ps -a
* docker rm 81609687ce62
* docker rmi spring-cloud/config-service


### Compile and Rebuild image ###

* docker rmi config-service
* ./mvnw clean package
* docker build -t config-service .

### Docker Compose commands ###

* docker-compose create config-service
* docker-compose up --no-start
* docker-compose start config-service
* docker-compose logs -f config-service
* docker ps
* docker-compose down


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
